package com.murad.a;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class SecondFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button klava = view.findViewById(R.id.button3);
        final TextView provod = view.findViewById(R.id.textView3);
        klava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                provod.setText("okey");
            }
        });

        Button monitor = view.findViewById(R.id.button4);
        final TextView monya = view.findViewById(R.id.textView4);
        monitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monya.setText("good");
            }
        });

        Button norm = view.findViewById(R.id.button5);
        final TextView text = view.findViewById(R.id.textView5);
        norm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText("otlichno");
            }
        });

        Button string = view.findViewById(R.id.button_second);
        final TextView view1 = view.findViewById(R.id.textView6);
        string.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view1.setText("grozniy");
            }
        });
        Button eleven = view.findViewById(R.id.button6);
        final TextView tekct7 = view.findViewById(R.id.textView8);
        eleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tekct7.setText("одиннацать");
            }
        });

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}